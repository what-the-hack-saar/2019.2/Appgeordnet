export interface petition{
    petitions_id : string,
    title : string,
    von : string,
    bis : string,
    anzahl_unterzeichner : string,
    link : string,
    begruendung : string,
    status : string
}