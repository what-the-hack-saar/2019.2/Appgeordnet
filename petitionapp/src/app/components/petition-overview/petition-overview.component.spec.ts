import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetitionOverviewComponent } from './petition-overview.component';

describe('PetitionOverviewComponent', () => {
  let component: PetitionOverviewComponent;
  let fixture: ComponentFixture<PetitionOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetitionOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetitionOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
