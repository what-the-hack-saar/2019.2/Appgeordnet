import { Component, OnInit } from '@angular/core';
import { petition } from 'src/datatypes/petition';
import { PetitionService } from 'src/services/PetitionService';

@Component({
  selector: 'app-petition-overview',
  templateUrl: './petition-overview.component.html',
  styleUrls: ['./petition-overview.component.css'],
  providers:  [ PetitionService ]
})
export class PetitionOverviewComponent implements OnInit {

  petitions : petition[];
  selectedPetition : petition;

  constructor(private petitionService: PetitionService) { }

  ngOnInit() {
    this.petitions = this.petitionService.GetPetitions();
  }

  selectPetition(selectedPetition: petition) { 
    this.selectedPetition = selectedPetition; 
  }

}
