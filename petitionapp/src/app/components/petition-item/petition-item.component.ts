import { Component, OnInit, Input, Output } from '@angular/core';
import { petition } from 'src/datatypes/petition';

@Component({
  selector: 'app-petition-item',
  templateUrl: './petition-item.component.html',
  styleUrls: ['./petition-item.component.css']
})
export class PetitionItemComponent {

 @Input() isSelected = false;
 @Input() petition: petition;

}
