import { Component, OnInit, Input } from '@angular/core';
import { petition } from 'src/datatypes/petition';

@Component({
  selector: 'app-petition-details',
  templateUrl: './petition-details.component.html',
  styleUrls: ['./petition-details.component.css']
})


export class PetitionDetailsComponent implements OnInit {
  @Input() petition: petition;
  
  constructor() { }

  ngOnInit() {
  }

}
