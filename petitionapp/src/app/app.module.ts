import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PetitionOverviewComponent } from './components/petition-overview/petition-overview.component';
import { PetitionDetailsComponent } from './components/petition-details/petition-details.component';
import { PetitionItemComponent } from './components/petition-item/petition-item.component';

@NgModule({
  declarations: [
    AppComponent,
    PetitionOverviewComponent,
    PetitionDetailsComponent,
    PetitionItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
