import json
import pandas as pd
from pymongo import MongoClient
import flask
from flask import Flask
import request
from flask_pymongo import PyMongo
import json
import requests
import pandas as pd
from bs4 import BeautifulSoup
import time
import datetime
from pymongo import MongoClient
from flask_pymongo import PyMongo
import os
from flask_cors import CORS


### status gibt abfrage
# 1: alle
# 2: zu unterzeichen
# 3: in untersuchung
# 4: abgelaufen

status = 2


def get_status_mapping(x):
    if x == '2':
        return('zu unterzeichnen')
    if x == '3':
        return('in der Pruefung')
    if x == '4':
        return('abgelaufen')

def return_petitionen(status):
    status = str(status)
    if status == "1":
        status = ['2','3','4']
    elif status > 1 and status < 5:
        status = [str(status)]
    else:
        return("The status setting does not meet the requirements. Needs to be an integer between 1 and 4.")

    petitionen = pd.DataFrame(columns=[
        'petitions_id',
        'title',
        'von',
        'bis',
        'anzahl_unterzeichner',
        'link',
        'begruendung',
        'category',
        'status'])


    for sta in status:
        url_ad="https://epetitionen.bundestag.de/epet/petuebersicht/mz.nc.content.teaser-petitionen-cards.$$$.ssi.true.status."+sta+".page.0.batchsize.80.html"
        response = requests.get(url_ad)

        soup = BeautifulSoup(response.content, 'html.parser')
        progress = soup.findAll(class_='progress')

        ts = [int(str(x.find(class_='progress-von').text))/1000 for x in progress]
        von = [datetime.datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S') for x in ts]

        ts = [int(str(x.find(class_='progress-bis').text))/1000 for x in progress]
        bis = [datetime.datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S') for x in ts]

        details = soup.findAll(class_='to-petdetails')
        title = [x.text for x in details]

        unterzeichnerAnzahl = soup.findAll(class_='util-icon--before util-icon--graph')
        unterzeichner = [x.span.text for x in unterzeichnerAnzahl]
        unterzeichner = [x.replace('\t','') for x in unterzeichner]
        unterzeichner = [x.replace('\r','') for x in unterzeichner]
        unterzeichner = [x.replace('\n','') for x in unterzeichner]
        unterzeichner = [int(x) for x in unterzeichner]

        pet_num = soup.findAll(class_='to-petdetails')
        ref_url = [x['href'] for x in pet_num]
        petition_number = [x.split('/') for x in ref_url]
        petition_number = [[x for x in y if x.find('Petition_') == 0] for y in petition_number]
        petition_number = [x[0] for x in petition_number]
        petition_number = [x.replace('.nc.html','') for x in petition_number]
        petition_number = [x.replace('Petition_','') for x in petition_number]

        category = soup.findAll(class_='teaser--label')
        category = [x.text for x in category]

        list_begruendung = []
        for x in ref_url:
            new_url = "http://epetitionen.bundestag.de" + x
            response = requests.get(new_url)
            new_soup = BeautifulSoup(response.content, 'html.parser')
            begruendung = new_soup.find(class_ = 'article-petition read-more-wrapper add-padding-md').text
            begruendung = begruendung.replace('Text der Petition','')
            begruendung = begruendung.replace('\n','')
            begruendung = begruendung.replace('\t','')
            begruendung = begruendung.replace('Mehr anzeigen Weniger anzeigenInfoBevor Sie über WhatsApp teilen können, installieren Sie bitte zunächst die WhatsApp-Version für Ihr EndgerätPetition teilen .','')
            begruendung = begruendung.replace('\r','')
            list_begruendung.append(begruendung)

        ref_url = ["http://epetitionen.bundestag.de" + x for x in ref_url]


        pets = pd.DataFrame(data = {
            'petitions_id' : petition_number,
            'title' : title,
            'von' : von,
            'bis' : bis,
            'anzahl_unterzeichner':unterzeichner,
            'link':ref_url,
            'begruendung':list_begruendung,
            'category':category,
            'status' : get_status_mapping(sta)
        })

        petitionen = petitionen.append(pets)

    print('Ich habe ' + str(petitionen.shape[0]) + ' Petitionen gefunden.'  )

    return(petitionen)

def fill_system():
    #client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
    #db = client.bundestagio.petitions

    mongo.db.pretitions.remove()
    petitions = return_petitionen(1)
    petitions = petitions.set_index('petitions_id',drop=False)

    print(petitions.head())
    for x in petitions.index.values:

        print('x '  + str(x))
        #pets.loc['98671'].to_dict()
        in_db = mongo.db.petitions.count_documents({'petitions_id': str(x)})
        print('in db: ' + str(in_db))
        if in_db == 0:
            print('fill system')
            print('dict :   ' + str(petitions.loc[x].to_dict()))
            mongo.db.petitions.insert_one(petitions.loc[x].to_dict())



def read_db(status,category):
    pets = pd.DataFrame(columns=[
        'petitions_id',
        'title',
        'von',
        'bis',
        'anzahl_unterzeichner',
        'link',
        'begruendung',
        'category',
        'status'])


    #client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
    #db = client.bundestagio.petitions

    #if mongo.db.petitions.count_documents({}) == 0:
    #    print('Ich mache erstmal die DB voll. Gib mir eine Minute.')
    #    fill_system()
    #else:
    #    print('Passt schon.')

    if status == 1:
        for x in mongo.db.petitions.find():
            pets = pets.append(dict(x), ignore_index=True)
    else:
        stat = get_status_mapping(str(status))
        if category:
            for x in mongo.db.petitions.find({'status':stat, 'category':category}):
                print(x)
                pets = pets.append((x), ignore_index=True)
        else:
            for x in mongo.db.petitions.find({'status':stat}):
                print(x)
                pets = pets.append((x), ignore_index=True)

    return(pets)


def return_petitions(status,category):
    petitions = read_db(status,category)
    print(petitions)
    return(petitions[['petitions_id',
        'title',
        'von',
        'bis',
        'anzahl_unterzeichner',
        'link',
        'begruendung',
        'category',
        'status']])


app = Flask(__name__)
app.config["DEBUG"] = True
app.config['MONGO_URI'] = os.environ.get('DB')
CORS(app)
mongo = PyMongo(app)

@app.route('/', methods=['GET'])
def get_pets():
    # Check which status was provided as part of the URL.
    # If status is provided, assign it to a variable.
    # If no status is not provided, display an error in the browser.

    if 'status' in flask.request.args:
        status = int(flask.request.args['status'])
        print(status)
    else:
        return("Error: No status field provided. Please specify an status between 1 and 4.")

    if 'category' in flask.request.args:
        category = flask.request.args['category']
    else:
        category = None

    print(status)

    results = return_petitions(status,category)
    print(results.to_json(orient = 'records'))
    return flask.jsonify(results.to_json(orient = 'records'))


if __name__ == '__main__':
    fill_system()
    app.run(debug=True, host='0.0.0.0')