import json
import requests
import pandas as pd
from bs4 import BeautifulSoup
import time
import datetime
from pymongo import MongoClient
from flask_pymongo import PyMongo


### status gibt abfrage
# 1: alle
# 2: zu unterzeichen
# 3: in untersuchung
# 4: abgelaufen

status = 2


def get_status_mapping(x):
    if x == '2':
        return('zu unterzeichnen')
    if x == '3':
        return('in der Pruefung')
    if x == '4':
        return('abgelaufen')

def return_petitionen(status):
    if status == "1":
        status = ['2','3','4']
    elif status > 1 and status < 5:
        status = [str(status)]
    else:
        return("The status setting does not meet the requirements. Needs to be an integer between 1 and 4.")

    petitionen = pd.DataFrame(columns=[
        'petitions_id',
        'title',
        'von',
        'bis',
        'anzahl_unterzeichner',
        'link',
        'begruendung',
        'category',
        'status'])

    for sta in status:
        url_ad="https://epetitionen.bundestag.de/epet/petuebersicht/mz.nc.content.teaser-petitionen-cards.$$$.ssi.true.status."+sta+".page.0.batchsize.100.html"
        response = requests.get(url_ad)

        soup = BeautifulSoup(response.content, 'html.parser')
        progress = soup.findAll(class_='progress')

        ts = [int(str(x.find(class_='progress-von').text))/1000 for x in progress]
        von = [datetime.datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S') for x in ts]

        ts = [int(str(x.find(class_='progress-bis').text))/1000 for x in progress]
        bis = [datetime.datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S') for x in ts]

        details = soup.findAll(class_='to-petdetails')
        title = [x.text for x in details]
        print(title)

        unterzeichnerAnzahl = soup.findAll(class_='util-icon--before util-icon--graph')
        unterzeichner = [x.span.text for x in unterzeichnerAnzahl]
        unterzeichner = [x.replace('\t','') for x in unterzeichner]
        unterzeichner = [x.replace('\r','') for x in unterzeichner]
        unterzeichner = [x.replace('\n','') for x in unterzeichner]
        unterzeichner = [int(x) for x in unterzeichner]
        print(unterzeichner)

        pet_num = soup.findAll(class_='to-petdetails')
        ref_url = [x['href'] for x in pet_num]
        petition_number = [x.split('/') for x in ref_url]
        petition_number = [[x for x in y if x.find('Petition_') == 0] for y in petition_number]
        petition_number = [x[0] for x in petition_number]
        petition_number = [x.replace('.nc.html','') for x in petition_number]
        petition_number = [x.replace('Petition_','') for x in petition_number]
        print(petition_number)

        category = soup.findAll(class_='teaser--label')
        category = [x.text for x in category]

        list_begruendung = []
        for x in ref_url:
            new_url = "http://epetitionen.bundestag.de" + x
            response = requests.get(new_url)
            new_soup = BeautifulSoup(response.content, 'html.parser')
            begruendung = new_soup.find(class_ = 'article-petition read-more-wrapper add-padding-md').text
            begruendung = begruendung.replace('Text der Petition','')
            begruendung = begruendung.replace('\n','')
            begruendung = begruendung.replace('\t','')
            begruendung = begruendung.replace('Mehr anzeigen Weniger anzeigenInfoBevor Sie über WhatsApp teilen können, installieren Sie bitte zunächst die WhatsApp-Version für Ihr EndgerätPetition teilen .','')
            begruendung = begruendung.replace('\r','')
            list_begruendung.append(begruendung)

        ref_url = ["http://epetitionen.bundestag.de" + x for x in ref_url]
        print(list_begruendung)


        pets = pd.DataFrame(data = {
            'petitions_id' : petition_number,
            'title' : title,
            'von' : von,
            'bis' : bis,
            'anzahl_unterzeichner':unterzeichner,
            'link':ref_url,
            'begruendung':list_begruendung,
            'category':category,
            'status' : get_status_mapping(stat)
        })

        petitionen = petitionen.append(pets)


    return(petitionen)

def fill_system():
    client = MongoClient('127.0.0.1:27017')
    db = client.bundestagio.petitions

    petitions = return_petitionen(1)

    for x in range(petitions.shape[0]):
        in_db = db.find({'petitions_id': petitions.petitions_id[x]}).count()
        if in_db == 0:
            db.insert_one(dict(petitions.loc[x, :]))

